@ECHO OFF
SET BASEDIR=%~dp0

REM Change this to Your Installed Distro Name
SET DISTRO="UbuntuCore"
SET WSLLAUNCH=%BASEDIR%%DISTRO%.exe

REM Validate Files.
IF NOT EXIST "%WSLLAUNCH%" ( CALL :WSLLAUNCH_MISSING )
REM UnInstall
CALL :UNINSTALL

CALL :QUIT
:WSLLAUNCH_MISSING

:QUIT
pause
exit

:UNINSTALL
ECHO Are you Sure you want to Uninstall the WSL Distro: %DISTRO%
ECHO Currently Installed to: %BASEDIR%
ECHO.
ECHO. If you Dont want To do this. hit CTRL+C TWICE NOW.
ECHO.
ECHO If your COMPLEATLY Sure You want to Proceed.
pause
wslconfig /u %DISTRO%
del %WSLLAUNCH%
