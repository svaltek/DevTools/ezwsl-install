# EzWSL-Install

Simple One-Click CustomWSL Installs

1: If youve never used WSL before then the feature is probably disabled.
    Run EnableWSL.cmd and REBOOT before you continue.

2: Copy install.cmd and the EzWSL folder to wherever you want the WSL Subsystem Installed
    (this is where your wsl distro will live and should be a folder with full read/write access for your account)

3: IMPORTANT: If youve changed the Distro to use (replace rootfs.img in EzWSL folder) tthen you need to edit INSTALL.cmd to Change The name for your WSL install.

4: run INSTALL.cmd it will copy the files into the right places and import the rootfs to wsl to install the chosen distribution
wait for installer to complete it will start wsl for you when done.

NOTE you should now run `apt update;apt upgrade -y;apt install -y wget curl nano tmux build-essential` to update your wsl distro and install some required apps

By Default ive included a rootfs for ubuntucore (the image is direct from  http://cdimage.ubuntu.com/ubuntu-core)
